	%%  Template using the document class for CRC1114 poster presentations, see crc1114-poster.cls
%%
%%  Corrections/suggestions to: p.gelss@fu-berlin.de
%%
%%  Class options:
%%
%%    - FU:    show logo of Freie Universitaet Berlin
%%    - GFZ:   show logo of Deutches GeoForschungsZentrum
%%    - MPIKG: show logo of Max-Planck-Institut fuer Kolloid- und Grenzflaechenforschung
%%    - TU:    show logo of Technische Universitaet Berlin
%%    - UP:    show logo of Universitaet Potsdam
%%    - WIAS:  show logo of Weierstrass-Institut fuer Angewandte Analysis und Stochastik
%%    - ZIB:   show logo of Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
%%
%%  Project, title, and authors are determined by the macros \setproject, \settitle, and 
%%  \setauthors, respectively.
%%
%%  To place poster boxes, use the command \posterbox[a](x,y)<w,h>{t}{c}, with: 
%%
%%    - a: additional arguments to pass to the TikZ node; optional
%%    - x: x-coordinate of the node, must be in [0, \paperwidth]
%%    - y: y-coordinate of the node, must be in [-\headerheight, -\paperheight]
%%    - w: width of the poster box
%%    - h: height of the poster box
%%    - t: title of the poster box; optional
%%    - c: content of the poster box
%%
%%  TikZ commands such as \node, \draw, \etc. can be directly used.


\documentclass[FU]{crc1114-poster}
\usepackage{physics}
\usepackage{xcolor}
%\usetikzlibrary{backgrounds}
%\usepackage{pgfplots}

%\newcommand{\key}[1]{\textcolor{red}{#1}}
\newcommand{\key}[1]{\textbf{#1}}

% Set project, title, and authors  (use \\ for multiple lines)
\setproject{Project A08}
\settitle{Lagrangian Coherence in Atmospheric Blocking}
\setauthors{\underline{Henry Schoeller}, Robin Chemnitz, Stephan Pfahl, P\' eter Koltai, Maximilian Engel}

\begin{document}

% Define margin and standard width for poster boxes
\newlength{\margin}
\setlength{\margin}{0.03\paperwidth}
\newlength{\boxwidth}
\setlength{\boxwidth}{0.455\paperwidth}


\posterbox[](\margin, -\headerheight-\margin)<0.95\boxwidth,13cm>{Lagrangian Coherence}{
		Suppose we have $m$ sample trajectories of a flow $\Phi^t$ in $\mathbb{R}^n$, evaluated at $T$ time instances 
$$ x_t^i:=\Phi^t x^i_{t_0} \in \mathbb{M}_t, \quad i\in \{1,\hdots, m\};\:t\in \{t_0, \hdots, t_{T-1}\}.$$
We want to find \key{coherent sets}, which are regions in state space that keep their geometric integrity to a large extent 
during temporal evolution.\\ 
$\rightarrow$ Find tight bundles of trajectories.
%This is equivalent to finding \textcolor{red}{tight bundles of trajectories} if the points forming the trajectories are conceived as samples from such a set.
}

\posterbox[](\margin, -\headerheight-2\margin-13cm)<0.95\boxwidth,12cm>{Diffusion Maps}{
	The evolution of coherent sets should be \key{insensitive to small perturbations}. Let $D_\epsilon$ denote the diffusion operator with variance $\epsilon$. We call $\mathbb{X}$ coherent if
	$$D_\epsilon \circ \left(\Phi^t\right)^{-1} \circ D_\epsilon \circ \Phi^t \: \mathbb{X} \approx \mathbb{X}.$$
	This heuristic is implemented using tranfer operators on the set of data-points. 
		%If $\Phi_{\epsilon, t}$ is the deterministic flow map plus some small random pertubation with variance $\epsilon$, \textcolor{red}{coherence implies robustness} in the sense of "$\Phi_{\epsilon, t}^{-1}(\Phi_{\epsilon, t} \mathbb{X}) \approx \mathbb{X}$", where $\mathbb{X} \subset \mathbb{R}^3$ is a coherent set at $t_0$. We make use of this by constructing a \textcolor{red}{diffusion operator on the data points}, whose eigenvectors give a low-dimensional representation which is used to extract coherent sets (\textcolor{red}{spectral clustering}) [1].
}

\posterbox[anchor=north east](\paperwidth-\margin, -\headerheight-\margin)<1.07\boxwidth,28.8cm>{Atmospheric Blocking}{
    High pressure blocking -- a.k.a.~Quasi-Stationary Atmospheric States(QSAS) -- are \key{critical features of mid-latitude weather} and,  importantly, associated with extreme events. Forecasting skill is, however, still unsatisfying. Physically, QSAS are characterized by \key{high stability}. 
\vfill    
\includegraphics[scale=1]{im/2016_00.pdf} \includegraphics[scale=0.8, viewport=0 0 375 450, clip]{im/blocktypes.png}\\
{\footnotesize Left: Blocking example with PV field (shaded), wind $>30 \mathrm{\frac{m}{s}}$ (blue) and blocking region (purple). Right: Point vortex model idealizations of $\Omega$- and High-over-Low-Blockings (from [2]).}
}

\posterbox[](\margin, -\headerheight-3\margin-25cm)<0.95\boxwidth,15.5cm>{Algorithm}{
		\begin{enumerate}
		\item Calculate trajectories $x_t^i$.
		\item Calculate pointwise diffusion similarity 
		$$\mathbf{K}_{\epsilon, t}(i,j) = \exp \left( -\epsilon^{-1}\|x^i_t - x^j_t\|^2 \right).$$ %with distance metric $D(\cdot,\cdot)$ 
		\item Calculate diffusion transition matrix $\mathbf{P}_{\epsilon, t}$ through normalization and applying boundary conditions.
		\item %Obtain space-time diffusion map 
		Averaged transition matrix $\mathbf{Q}_{\epsilon} = \frac{1}{T} \sum_t \mathbf{P}_{\epsilon, t}$.
		\item Perform spectral clustering on $\mathbf{Q}_{\epsilon}$.
		\end{enumerate}
}

\posterbox[anchor=north east](\paperwidth-\margin, -\headerheight-2\margin-28.8cm)<1.05\boxwidth,52.7cm>{Trajectory Density}{
	Assume at time $t$ the points $x_t^i$ are sampled from some manifold $\mathbb{M}_t$. Then, the sum of the diffusion similarities can be interpreted as a \key{Monte Carlo integral approximation} [4]
\begin{align*}S_{\epsilon, t} := \sum_{i,j}\mathbf{K}_{\epsilon, t}(i,j) \approx \frac{m^2}{\mathrm{vol}(\mathbb{M}_t)^2} \int_{\mathbb{M}_t}\int_{\mathbb{M}_t} \exp (-\epsilon^{-1} \|x-y\|^2) \dd x \dd y \approx \frac{m^2}{\mathrm{vol}(\mathbb{M}_t)} (2 \pi \epsilon)^{d(t)/2}
\end{align*}
%$S_{\epsilon, t}$ is a measure of the \textcolor{red}{density} of points, with $\lim_{{\epsilon \to 0}} S_{\epsilon, t}= m$ and $\lim_{{\epsilon \to \infty}}S_{\epsilon, t} = m^2$. Also $\log(S_{\epsilon, t}) \approx \frac{d}{2}\log(\epsilon) + \log(\frac{(2\pi)^{d/2} m^2}{\mathrm{vol}(\mathbb{M}_t)})$.
For suited $\epsilon$, this yields a linear relationship between $\log(\epsilon)$ and $\log(S_{\epsilon, t})$.
$$\log(S_{\epsilon, t}) = \frac{d(t)}{2} \log(\epsilon) + \log(\rho(t)) + \log(m) + \frac{d(t)}{2} \log(2 \pi); \quad \rho(t) := \frac{m}{\mathrm{vol}(\mathbb{M}_t)}.$$
\vspace{16cm}\\
The dimension $d(t)$ and density $\rho(t)$ can be computed from this graph. We use $\ell(t):=\frac{1}{d(t)} \log(\rho(t))$ as a measure of density. The trajectories tend to be \key{denser after passage} through the QSAS, which is in line with the established notion of blockings as regions characterized by high stability.	
%We obtain $d = \dim (\mathbb{M}_t)$ by maximizing $\frac{\dd \log (S_{\epsilon, t})}{\dd \log (\epsilon)}$ and calculate the point density measure $\rho (t) := \frac{1}{d}\log (\frac{m}{\mathrm{vol}(\mathbb{M}_t)})$.
}

\node[anchor=north west] (epsloglog) at (\paperwidth-\margin-1.06\boxwidth, -\headerheight-2\margin-44.5cm) {\includegraphics[scale=1]{im/20160502_00.pdf}};
\node[anchor=north west] (trajs) at (\paperwidth-\margin-0.4\boxwidth, -\headerheight-2\margin-47.515cm) {\includegraphics[scale=1]{im/traj_20160502_00.pdf}};
%\begin{tikzpicture}
    % First image (bottom layer)
%    \node[anchor=north west,inner sep=0] (image1) at (0,0) {\includegraphics[scale=1]{im/20160502_00.pdf}};    
    % Second image (top layer)
%    \node[anchor=north west,inner sep=0, opacity=1] (image2) at (10,-3) {\includegraphics[scale=1]{im/trajs.pdf}};
%\end{tikzpicture} 

\node[anchor=north west] (dimension) at (\paperwidth-\margin-1.05\boxwidth, -\headerheight-2\margin-67cm) {\includegraphics[scale=1]{im/dimensionall.pdf}};
\node[anchor=north west] (density) at (\paperwidth-\margin-0.5\boxwidth, -\headerheight-2\margin-67cm) {\includegraphics[scale=1]{im/densityall.pdf}};

\posterbox[](\margin, -\headerheight-4\margin-40.5cm)<0.95\boxwidth,10cm>{Boundary Conditions}{
Flow across the boundary $\partial \mathbb{M}_t$ is possible. We apply \key{Dirichlet boundary conditions}
$$\mathbf{P}_{\epsilon, t}(i, j) = 0 \quad \forall x^i_t \text{ or } x^j_t \in \partial \mathbb{M}_t$$
The boundary is detected with $\alpha$\texttt{-shapes} [3] using $\alpha \approx \mathcal{O}(\epsilon^{-1/2})$.
}

\posterbox[](\margin, -\headerheight-5\margin-50.5cm)<0.95\boxwidth,\paperheight-\headerheight-6\margin-50.5cm>{Spectral Clustering}{
	To study the \key{airstreams entering} the QSAS, sample $x_0^i$ from the QSAS. Construct $\mathbf{Q}_{\epsilon}$ and compute its dominant eigenvalues.\\
$\rightarrow$ Find spectral gap.\\
With $k$-means, group the points into $k$ clusters, using the $k-1$ dominant eigenvectors. \vspace{14.2cm}\\
\parbox{.42\boxwidth}{
The clustered trajectories differ not only wrt their \key{geometric}, but also in their \key{dynamic properties}. Specifically, two coherent sets are identified (red and pink), which feature strong vertical, cross-isentropic motion (latent heating) and \key{stabilize the QSAS} via injection of low-PV air masses. Objectively identifying this process presents an advancement of existing research [5].}
}


\node[anchor=north west] (spectrum) at (\margin+0.1cm, -\headerheight-5\margin-59cm) {\includegraphics[scale=1]{im/spectrum.pdf}};

\node[anchor=north west] (clustering) at (\margin+17.2cm, -\headerheight-5\margin-57.5cm) {\includegraphics[scale=1,  trim={11cm 6.5cm 5.8cm 3.5cm}, clip]{im/3d.pdf}};

\node[anchor=south east] (theta) at (\margin+0.95\boxwidth, -\paperheight+\margin+0.1cm) {\includegraphics[scale=1]{im/test.pdf}};

\posterbox[anchor=north east](\paperwidth-\margin, -\headerheight-3\margin-81.5cm)<1.07\boxwidth,\paperheight-\headerheight-4\margin-81.5cm>{References}{
    \scriptsize
    \hspace*{0.5cm} [1] \textsc{Banisch, Ralf and Koltai, P\'eter}, \textit{Understanding the Geometry of
Transport: Diffusion Maps for Lagrangian Trajectory Data Unravel Coherent Sets}, Chaos 27.3, 2017\\[0.5cm]
    \hspace*{0.5cm} [2] \textsc{Detring, Carola et al.}, \textit{Occurrence and Transition Probabilities
of Omega and High-over-Low Blocking in the Euro-Atlantic Regio}, Weather Clim. Dynam. 2.4, 2021\\[0.5cm]
    \hspace*{0.5cm} [3] \textsc{Edelsbrunner, Herbert and Mücke, Ernst P.}, \textit{Three-Dimensional
Alpha Shapes}, ACM Trans. Graph. 13.1, 1994\\[0.5cm]
    \hspace*{0.5cm} [4] \textsc{Koltai, P\' eter and Weiss Stephan}, \textit{Diffusion Maps Embedding and
Transition Matrix Analysis of the Large-Scale Flow Structure in Turbulent
Rayleigh–B\' enard Convectio}, Nonlinearity 33.4, 2020 \\[0.5cm]
	\hspace*{0.5cm} [5] \textsc{Pfahl, Stephan et al.},  \textit{Importance of Latent Heat Release in Ascending Air Streams for Atmospheric Blocking}, Nature Geoscience 8, no. 8, 2015
}

\end{document}

%% Document class for CRC1114 poster presentations
%%
%% Updated October 2023
%%
%% Corrections/suggestions to: p.gelss@fu-berlin.de
%%
%%
%% Class options:
%%
%% - FU:    show logo of Freie Universitaet Berlin
%% - BTU:   show logo of Brandenburgische Technische  Universitaet Cottbus-Senftenberg
%% - UP:    show logo of Universitaet Potsdam
%% - WIAS:  show logo of Weierstrass-Institut fuer Angewandte Analysis und Stochastik
%% - ZIB:   show logo of Konrad-Zuse-Zentrum fuer Informationstechnik Berlin


% I D E N T I F I C A T I O N
% ===========================

% Set LaTeX version
\NeedsTeXFormat{LaTeX2e}

% Identify class
\ProvidesClass{crc1114-poster}

% Load a0poster class
\LoadClass[portrait,a0,final]{a0poster}


% P A C K A G E S
% ===============

% set margins to zero
\RequirePackage[margin=0cm]{geometry}
\RequirePackage[mathtypeface=default:scale:1.5,
            sanstypeface=helvetica:scale:1.5, 
            monotypeface=cm:scale:1.75]{typeface}
\RequirePackage[english]{babel}
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage[export]{adjustbox}
\RequirePackage{tikz}
\usetikzlibrary[shapes,arrows, positioning]
\RequirePackage{graphicx}
\RequirePackage[nomessages]{fp}
\RequirePackage{forloop}
\RequirePackage{color}
\RequirePackage{xparse}


% C O L O R   T A B L E
% =====================

% Define standard colors
\definecolor{Blue} {RGB}{38, 89, 140}
\definecolor{Green}{RGB}{178, 204, 51}
\definecolor{Gray} {RGB}{230, 230, 230}


% P A R A M E T E R S
% ===================

% Change the default font family to sans-serif
\renewcommand{\familydefault}{\sfdefault}

% Disable automatic indentiation
\setlength{\parindent}{0cm}

% Suppress \hbox warning
\hbadness=99999


% C O U N T E R S
% ===============

% Logo counter
\newcounter{logocounter}
\setcounter{logocounter}{1}

% Auxiliary counters
\newcounter{i}
\newcounter{j}


% L E N G T H S
% =============

% Minimum heights of header boxes
\newlength{\hinner}
\setlength{\hinner}{11cm}
\newlength{\houter}
\setlength{\houter}{\dimexpr(\hinner + 2cm)\relax}

% Gap width/height
\newlength{\gap}
\setlength{\gap}{0.275cm}

% Top of the content node (for later use)
\newlength{\headerheight}
\setlength{\headerheight}{\dimexpr(\houter+1.5cm+\gap)\relax}

% Width of logo boxes
\newlength{\logobox}
\setlength{\logobox}{0.15\paperwidth}


% D E F I N I T I O N S
% =====================

% Define string for project, title, and authors
\def\setproject#1{\newcommand{\StrProject}{#1}}
\def\settitle#1{\newcommand{\StrTitle}{#1}}
\def\setauthors#1{\newcommand{\StrAuthors}{#1}}

% Generate array containing logos, names, and URLs of involved institutions
\def\addlogo#1#2#3{
  \expandafter\newcommand\csname InstLogo\the\value{logocounter} \endcsname{#1} 
  \expandafter\newcommand\csname InstName\the\value{logocounter} \endcsname{#2} 
  \expandafter\newcommand\csname InstURL\the\value{logocounter} \endcsname{#3} 
  \stepcounter{logocounter}
}

% Create title box
\def\TitleBox{

    % Position of title box
    \def\x{0.15*\paperwidth + 0.275cm}

    % Width of title box
    \newlength{\w}
    \setlength{\w}{\dimexpr(\paperwidth - \the\value{logocounter}\logobox - \the\value{logocounter}\gap - 2cm)\relax}
    
    % Measure height needed for project, title, and authors
    \sbox{0}{
        \parbox[t]{0.99\w}{
            \Large\bf\StrProject
            \vfill
            \fontsize{62pt}{100pt}\selectfont\bf\StrTitle
            \vfill
            \vspace*{1cm}
            \normalsize\StrAuthors\\
        }
    }
    
    % Increase minimum height of title box if necessary
    \ifdim\dimexpr\ht0+\dp0>\hinner
        \setlength{\hinner}{\dimexpr(\ht0 + \dp0)\relax}
        \setlength{\houter}{\dimexpr(\hinner + 2cm)\relax}
        \setlength{\headerheight}{\dimexpr(\houter+1.5cm+\gap)\relax}
    \fi

    % Create title box
    \node[fill=Green, minimum width=\w+2cm,text width=\w, anchor=north west, minimum height=\houter, align=center] (HeaderCenter) at (\x,0) {
        % Use parbox to display project, title, and authors
        \parbox[t][\hinner]{0.99\w}{
            % Print project
            \Large\bf\strut\textcolor{white}{\StrProject}
            \vfill
            % Print title
            \fontsize{62pt}{100pt}\selectfont\bf\strut\textcolor{white}{\StrTitle}
            \vfill
            \vspace*{1cm}
            % Print authors
            \normalsize\strut\textcolor{white}{\StrAuthors}
        }
    };
}

% Create logo box
\def\InstitutionBox{

    \ifnum\the\value{i}=0
        % Print CRC logo
        \node[fill=Blue, minimum width=0.15*\paperwidth, minimum height=\houter, anchor=north west] (HeaderLeft) at (0,0) {
            \parbox[c][\houter][c]{0.14\paperwidth}{
                \centering
                \vfill
                % Print logo
                \includegraphics[width=0.1\paperwidth]{logos/logo_crc.pdf}
                \vfill
                % Print name
                \footnotesize\strut\textcolor{white}{CRC 1114}
                \vfill
                \footnotesize\strut\textcolor{white}{Scaling Cascades in \\[0.2cm] Complex Systems}
                \vfill
            }
        };
    \else
        % Print institution logo
        \def\x{1.15*\paperwidth + 0.275cm - \the\value{i}*0.15*\paperwidth - \the\value{i}*0.275cm}
        \setcounter{j}{\the\value{logocounter}}
        \addtocounter{j}{-\the\value{i}}
        \node[fill=Blue, minimum width=0.15*\paperwidth, minimum height=\houter, anchor=north east] (Box) at (\x,0) {
            % Use parbox to display logo, name, and URL
            \parbox[c][\houter][c]{0.14\paperwidth}{
                \centering
                \vfill
                % Print logo
                \parbox[c][0.09\paperwidth][c]{0.09\paperwidth}{\centering\includegraphics[width=0.09\paperwidth, max height=0.09\paperwidth]{\csname InstLogo\arabic{j} \endcsname}}
                \vfill
                % Print institution name and URL
                \footnotesize\strut\textcolor{white}{\csname InstName\arabic{j} \endcsname}
                \footnotesize\tt\strut\textcolor{white}{\csname InstURL\arabic{j} \endcsname}
                \vfill
                }
        };
    \fi
}

% Create content box
\def\ContentBox{
    \node[fill=Gray, minimum width=\paperwidth, minimum height=\paperheight-\houter-0.275cm, anchor=north west] at (0,-\houter-0.275cm) {};
}

% Create poster box
\def\posterbox[#1](#2,#3)<#4,#5>#6#7{
    % Print content box
    \node[anchor=north west, draw=black, line width=0.1cm, fill=white, minimum width=#4, minimum height=#5, align=left, inner sep=2cm, #1] (Box) at (#2,#3) {
        \parbox[c][\dimexpr(#5-4.2cm)\relax][t]{\dimexpr(#4-4.2cm)\relax\small\linespread{1.3}\selectfont}{#7}
    };
    \ifx&#6&%
    \else
    % Print title box
    \node[fill=Green, anchor=west, rounded corners=10pt, minimum height=2.5cm, right=1cm of Box.north west, text depth=0pt] {
        \hspace*{1cm} \bf\textcolor{white}{#6}\hspace*{1cm}
    };
    \fi
}


% C L A S S   O P T I O N S
% =========================

% Add logos
\DeclareOption{FU}{\addlogo{logos/logo_fu}{Freie Universit\"at Berlin}{www.fu-berlin.de}}
\DeclareOption{BTU}{\addlogo{logos/logo_btu}{}{https://www.b-tu.de/}} %Brandenburgische Technische  Universit\"at Cottbus-Senftenberg
\DeclareOption{UP}{\addlogo{logos/logo_up}{Universit\"at Potsdam}{www.uni-potsdam.de}}
\DeclareOption{WIAS}{\addlogo{logos/logo_wias}{Weierstra\ss-Institut f\"ur Angewandte Analysis und Stochastik}{www.wias-berlin.de}}
\DeclareOption{ZIB}{\addlogo{logos/logo_zib}{Konrad-Zuse-Zentrum f\"ur Informationstechnik Berlin}{www.zib.de}}
\ProcessOptions*\relax


% B E G I N   A N D   E N D   O F   D O C U M E N T
% =================================================

\AtBeginDocument{

    % Open tikzpicture environment
    \begin{tikzpicture}[inner sep=0pt, outer sep=0pt]
    
    % Clip to poster
    \clip(0,0) rectangle (\paperwidth, -\paperheight);

    % Print title box
    \TitleBox

    % Print institution logos
    \forloop{i}{0}{\value{i}<\value{logocounter}}{\InstitutionBox}
    
    % Create content box
    \ContentBox
    }

\AtEndDocument{

    % Close tikzpicture environment at the end of the document
    \end{tikzpicture}
}

